const path = require( 'path' );
const env = require('dotenv').config();
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const cityData = require('./cities.json');

module.exports = {
	mode: process.env.NODE_ENV || 'production',
	entry: './src/index.js',
  	watch: true,
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: 'bundle.js'
	},
	module: {
		rules: [   
			{
				test: /\.json$/, 
				loader: 'json' 
			},
		    {
			    test: /\.js$/,
			    exclude: /(node_modules)/,
			    loaders: ['babel-loader'],
			},
		    {
		      	test: /\.(sa|sc|c)ss$/,
		      	use: [
		      		{
		               loader: MiniCssExtractPlugin.loader
		           	},
	             	{
		               loader: "css-loader",
	             	},
	             	{
		               loader: "postcss-loader"
	             	},
	             	{
		               	loader: "sass-loader"
	             	}
	           	]
		    }
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'bundle.css'
		}),
		new HtmlWebpackPlugin({
			templateParameters: (compilation, assets, assetTags, options) => {
		        return {
		          	compilation,
		          	webpackConfig: compilation.options,
		          	htmlWebpackPlugin: {
		            	tags: assetTags,
		            	files: assets,
		            	options
		          	},
		          	'cities': cityData.cities
		        };
	      	},
			template: './src/index.ejs'
		})
	]
};
