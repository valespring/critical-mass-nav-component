# Apple Nav Component

For Critical Mass for Apple.

## Tech Used

- Webpack
- EJS Templating with JSON import

## What It Does

- Selects the city, with hover color to indicate useability
- Slides indicator to show relationship
- Loads time into target element

## Thought Process

I took the scenic route on this one by seeing if I could pass the data from the JSON file into the front-end.

Coming from a Shopify background, we often have to leverage the templating language in interesting ways. One thing that you'll see used somewhat heavily in this example is the usage of data attributes, so I can access certain selectors and their relationship instead of declaring a multitude of variables.

Instead of a window `resize` function being called to update the position, I really opted for CSS functionality being the forefront. I am checking for the sum of the items being greater than the parent container size, and only then am I setting its width. If the width is set, a user should be able to scroll through these items horizontally and select the city.

## Build

`npm run build`

Access dist folder.