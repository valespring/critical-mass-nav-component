const config = {
	tabElems: document.querySelectorAll('.js-tab'),
	tabList: document.querySelector('.js-tabs-list'),
	indicatorElem: document.querySelector('.js-indicator'),
	targetElem: document.querySelector('.js-target'),
	currentTab: null,
	itemsWidth: 0,
	tabsWidth: 0,
	currentOffset: 0,
	activeClass: 'is-active'
};

export default config;