import config from './config';

const script = {
	setTabPosition: function() {
		config.indicatorElem.style.width = `${config.currentTab.offsetWidth}px`;
		config.indicatorElem.style.left = `${config.currentTab.offsetLeft}px`;
	},

	changeTargetTime: function() {
		let currentDate = new Date();
		let offset = Number(config.currentTab.dataset.offset);

		let hours = currentDate.getUTCHours() + offset;
		let minutes = currentDate.getUTCMinutes();
		let revisedMinutes = minutes.toString().length != 2 ? `0${minutes}` : minutes;

		let htmlTime = `${hours}:${revisedMinutes}GMT`;

		config.targetElem.innerHTML = htmlTime;
	},

	setTabStatus: function() {
		let formerSelectedTab = Array.from(config.tabElems).filter((elem)=>{
			return elem.classList.contains(config.activeClass)
		})[0];

		if ( formerSelectedTab ) formerSelectedTab.classList.remove(config.activeClass);
		config.currentTab.classList.add(config.activeClass);
	},

	setContainerSize: function () {
		let parentContainerWidth = config.tabList.closest(config.tabList.dataset.parent).offsetWidth;

		if ( config.itemsWidth != 0 && config.itemsWidth > parentContainerWidth && !config.tabList.style.width) {
			config.tabList.style.width = `${config.itemsWidth}px`;
		}
	},

	calculateWidth: function ( elem ) {
	  	let width = elem.offsetWidth;
 	 	let style = getComputedStyle(elem);

	  	width += parseInt(style.marginLeft) + parseInt(style.marginRight);

	  	return width;
	},

	tabEventListener: function(e) {
		let gmtOffset = Number(e.target.dataset.offset);
		let revealTarget = e.target.dataset.target;

		let target = e.target;

		config.currentTab = e.target;

		this.changeTargetTime();
		this.setTabPosition();
		this.setTabStatus();
	},

	setupComponent: function () {
		config.currentTab = config.tabElems[0];

		for (let i = config.tabElems.length - 1; i >= 0; i--) {
			let tab = config.tabElems[i];
			let self = this;

			tab.addEventListener('click', function (e) {
				self.tabEventListener(e);
			}.bind(this));

			let tabWidth = self.calculateWidth(tab.closest(tab.dataset.parent));

			config.itemsWidth += tabWidth;
		}

		this.setContainerSize();
		this.setTabPosition();
		this.setTabStatus();
		this.changeTargetTime();
	},

	resizeListener: function () {
		let self = this;

		window.onresize = ()=>{
			self.setContainerSize();
		}
	}
}

export default script;